/* Ответ на теоретический вопрос:
Цикл forEach бежит по всем элементам массива, и при этом применяет к ним алгоритм описанный в функции callback, которую forEach принимает в качестве аргумента.
*/

const array = ["Greta", "nach Haus", 23, null, true, undefined, user = {name: "Seth", surname: "Cohen"}, Symbol("something")];

//через метод filter:
function filterBy (array, type) {
	const newArray = array.filter(function (element){
		if (typeof(element) !== type){
			return true;
		} else {
			return false;
		}
	});
	return newArray;
}


//через forEach:
/*function filterBy (array, type) {
	let newArray = [];
	array.forEach (function(element){
			if(typeof(element) !== type){
			newArray.push(element);
		}
	}) 
	return newArray;
}*/


const allTypes = ['string', 'number', 'boolean', 'undefined', 'object', 'symbol'];

allTypes.forEach(type => console.log(filterBy(array, type)));



